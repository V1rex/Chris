# Chris
 Chris is an android app, that let you translate any text from more than 50 languages to other languages . 

![Icon](https://i.imgur.com/UNM79yx.png)

This app uses Yandex Translate web service 
for more informations checkout : https://tech.yandex.com/translate/doc/dg/concepts/api-overview-docpage/
[![Yandex Logo](https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Yandex_logo_en.svg/2000px-Yandex_logo_en.svg.png)]()

UML Diagram of the app : 

[![UML Diagram](https://i.imgur.com/Q43vnhP.jpg)]()
  - MainActivity : An activity that displays a list of old translated text and make you able to translate new text from a language to another . 
  - TranslateService : A service that is started from MainActivity , it is reposnible for making web request with yandex service and translating the text . 
  - TranslateAdapter : A class responsible for adapting layout in RecyclerView that displays a list of old translated text in MainActivity .
  - Translated : A class responsible for storing the informations about a translated text 
  - SettingsActivity : An Activity that make you able to change the languages preferences of your text that you wanna translate.
  - Utilities : a class that contains methods used in the app 
  - TranslateProvider/TranslateContract/TranslateDbHelper : Classes responsible that manages the content provider in the app that stores a list of old translated text  
# Code examples :
  - This app uses a google library for making easly json request in TranslateService
  for more informations : https://developer.android.com/training/volley
  - This app show how to use the yandex translate api : https://translate.yandex.net/api/v1.5/tr.json/translate
  - This app shows how to use RecyclerView for displaying a list 
  for more informations : https://developer.android.com/guide/topics/ui/layout/recyclerview


