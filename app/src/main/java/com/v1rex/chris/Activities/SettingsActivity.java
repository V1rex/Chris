package com.v1rex.chris.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.v1rex.chris.R;
import com.v1rex.chris.Utils.Utilities;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings");


        // if their is no inputLanguage selected,  English will be selected as inputLanguage with id : 0
        String inputLanguage = Utilities.getStringSharedPreferences(Utilities.mLanguageInputSharedPref , this);
        if (inputLanguage == null) {
            Utilities.setStringSharedPreferences(Utilities.mLanguageInputSharedPref , Languages.mLanguages.get(0) , this);
//            editor.putString(SettingsActivity.mLanguageInputSharedPref ,  Languages.mLanguages.get(0));
            Utilities.setStringSharedPreferences(Utilities.mLanguageInputCodeSharedPref  ,Languages.mLanguagesCodes.get(0) , this );
//            editor.putString(SettingsActivity.mLanguageInputCodeSharedPref ,  Languages.mLanguagesCodes.get(0));
            Utilities.setIntSharedPreferences(Utilities.mLanguageInputIdSharedPref , 0 , this);
//            editor.putInt(SettingsActivity.mLanguageInputIdSharedPref , 0);
//            editor.apply();
        }

        // if their is no outputLanguage selected , French will be selected as inputLanguage with id : 1
        String outputLanguage = Utilities.getStringSharedPreferences(Utilities.mLanguageOutputSharedPref, this);
        if(outputLanguage == null ){
            Utilities.setStringSharedPreferences(Utilities.mLanguageOutputSharedPref  , Languages.mLanguages.get(1) , this);
            Utilities.setStringSharedPreferences(Utilities.mLanguageOutputCodeSharedPref , Languages.mLanguagesCodes.get(1) , this);
            Utilities.setIntSharedPreferences(Utilities.mLanguageOutputIdSharedPref , 1, this);
        }


        // get the current selected input and output language id

        int inputLanguageId = Utilities.getIntSharedPreferences(Utilities.mLanguageInputIdSharedPref , this);
        int outputLanguageId = Utilities.getIntSharedPreferences(Utilities.mLanguageOutputIdSharedPref , this);

        // settings spinners for choosing input language and output language
        Spinner spinnerInputLanguage = (Spinner) findViewById(R.id.input_language_spinner);

        Spinner spinnerOutputLanguage = (Spinner) findViewById(R.id.output_language_spinner);



        // set the lists that will be showen in input langauges and output languages spinners
        ArrayAdapter<String> dataAdapterInput = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Languages.mLanguages);

        dataAdapterInput.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        spinnerInputLanguage.setAdapter(dataAdapterInput);

        spinnerInputLanguage.setSelection(inputLanguageId);


        spinnerOutputLanguage.setAdapter(dataAdapterInput);

        spinnerOutputLanguage.setSelection(outputLanguageId);



        // handling selected inputLanguages
        spinnerInputLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //get the selected languages from the spinner
               String language = Languages.mLanguages.get(i);
               String languageCode = Languages.mLanguagesCodes.get(i);

                String outputLanguage = Utilities.getStringSharedPreferences(Utilities.mLanguageOutputSharedPref, getBaseContext());

               // checking if the same language is not re-selected
                if(language.equals(outputLanguage)){
                    Toast.makeText(getBaseContext() , "Sorry but you can't choose the same language", Toast.LENGTH_SHORT ).show();
                }else{
                    // updating the selected input language if everything is okay
//                    editor.putString(mLanguageInputSharedPref , language);
                    Utilities.setStringSharedPreferences(Utilities.mLanguageInputSharedPref , language , getBaseContext());
//                    editor.putString(mLanguageInputCodeSharedPref, languageCode);
                    Utilities.setStringSharedPreferences(Utilities.mLanguageInputCodeSharedPref , languageCode , getBaseContext());
//                    editor.putInt(mLanguageInputIdSharedPref, i);
                    Utilities.setIntSharedPreferences(Utilities.mLanguageInputIdSharedPref , i,  getBaseContext());
//                    editor.apply();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });

        // handling selected outputLanguages
        spinnerOutputLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //get the selected languages from the spinner
                String language = Languages.mLanguages.get(i);
                String languageCode = Languages.mLanguagesCodes.get(i);
                String inputLanguage = Utilities.getStringSharedPreferences(Utilities.mLanguageInputSharedPref , getBaseContext());

                // checking if the same language is not re-selected
                if (language.equals(inputLanguage)){
                    Toast.makeText(getBaseContext() , "Sorry but you can't choose the same language", Toast.LENGTH_SHORT ).show();
                }else{
                    // updating the selected output language if everything is okay
                    Utilities.setStringSharedPreferences(Utilities.mLanguageOutputSharedPref , language , getBaseContext());
                    Utilities.setStringSharedPreferences(Utilities.mLanguageOutputCodeSharedPref , languageCode , getBaseContext());
                    Utilities.setIntSharedPreferences(Utilities.mLanguageOutputIdSharedPref , i,  getBaseContext());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SettingsActivity.this , MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home){
            Intent intent = new Intent(SettingsActivity.this , MainActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    ///this is a static class that stores an ArrayList of Languages Names and Languages Codes with same id
    public static class Languages{
        public static final ArrayList<String> mLanguagesCodes = new ArrayList<String>() {{
            add("en"); add("fr"); add("az"); add("sq"); add("am"); add("ar"); add("hy"); add("af");add("eu");
            add("ba"); add("be"); add("bn"); add("my"); add("bg"); add("bs"); add("cy"); add("hu");
            add("vi"); add("ht"); add("gl"); add("nl"); add("mrj"); add("el"); add("ka"); add("gu");
            add("da"); add("he"); add("yi"); add("id"); add("ga"); add("it"); add("is"); add("es");
            add("kk"); add("kn"); add("ca"); add("ky"); add("zh"); add("ko"); add("xh"); add("km");
            add("lo"); add("la"); add("lv"); add("lt"); add("lb"); add("mg"); add("ms"); add("ml");
            add("mt"); add("mk"); add("mi"); add("mr"); add("mhr"); add("mn"); add("de"); add("ne");
            add("no"); add("pa"); add("pap"); add("fa"); add("pl"); add("pt"); add("ro"); add("ru");
            add("ceb"); add("sr"); add("si"); add("sk"); add("sl"); add("sw"); add("su"); add("tg");
            add("th"); add("tl"); add("ta"); add("tt"); add("te"); add("tr"); add("udm"); add("uz");
            add("uk"); add("ur"); add("fi"); add("hi"); add("hr"); add("cs"); add("sv");
            add("gd"); add("et"); add("eo"); add("jv"); add("ja");

        }};

        public static final ArrayList<String> mLanguages = new ArrayList<String>() {{
            add("English");  add("French");  add("Azerbaijan"); add("Albanian"); add("Amharic");
            add("Arabic"); add("Armenian"); add("Afrikaans"); add("Basque");
            add("Bashkir"); add("Belarusian"); add("Bengali"); add("Burmese");
            add("Bulgarian"); add("Bosnian"); add("Welsh"); add("Hungarian");
            add("Vietnamese"); add("Haitian (Creole)"); add("Galician"); add("Dutch");
            add("Hill Mari"); add("Greek"); add("Georgian"); add("Gujarati");
            add("Danish"); add("Hebrew"); add("Yiddish"); add("Indonesian");
            add("Irish"); add("Italian"); add("Icelandic"); add("Spanish");
            add("Kazakh"); add("Kannada"); add("Catalan"); add("Kyrgyz");
            add("Chinese"); add("Korean"); add("Xhosa"); add("Khmer");
            add("Laotian"); add("Latin"); add("Latvian"); add("Lithuanian");
            add("Luxembourgish"); add("Malagasy"); add("Malay"); add("Malayalam");
            add("Maltese"); add("Macedonian"); add("Maori"); add("Marathi");
            add("Mari"); add("Mongolian"); add("German"); add("Nepali");
            add("Norwegian"); add("Punjabi"); add("Papiamento"); add("Persian");
            add("Polish"); add("Portuguese"); add("Romanian"); add("Russian");
            add("Cebuano"); add("Serbian"); add("Sinhala"); add("Slovakian");
            add("Slovenian"); add("Swahili"); add("Sundanese"); add("Tajik");
            add("Thai"); add("Tagalog"); add("Tamil"); add("Tatar");
            add("Telugu"); add("Turkish"); add("Udmurt"); add("Uzbek");
            add("Ukrainian"); add("Urdu"); add("Finnish");
            add("Hindi"); add("Croatian"); add("Czech"); add("Swedish");
            add("Scottish"); add("Estonian"); add("Esperanto"); add("Javanese"); add("Japanese");

        }};
    }
}
