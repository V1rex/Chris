package com.v1rex.chris.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.v1rex.chris.Adapters.TranslateAdapter;
import com.v1rex.chris.Database.TranslateContract;
import com.v1rex.chris.Models.Translated;
import com.v1rex.chris.R;
import com.v1rex.chris.Services.TranslateService;
import com.v1rex.chris.Utils.Utilities;

import java.util.ArrayList;
import java.util.List;




/**
 * This an example code of Activity
 */
public class MainActivity extends AppCompatActivity {

    private LocalBroadcastManager bManager;
    private Intent i;
    private EditText mEditText , mTranslatedText;
    private TextView mLanguageInputText , mLanguageOutputText;
    private Button mTranslateBtn;
    private RecyclerView mTranslatedRecyclerView;
    private RecyclerView.Adapter mTranslatedAdapter;

    private String inputLanguage , outputLanguage;

    public static final String RECEIVE_JSON = "com.v1rex.chris.RECEIVE_JSON";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set a title for this Activity that will be showen in the user interface
        getSupportActionBar().setTitle("Translate");

        mEditText = (EditText) findViewById(R.id.edit_text);
        mTranslatedText = (EditText) findViewById(R.id.edit_translated_text);


        // set inputLanguages for default if the user has not choosed an input Language yet
        inputLanguage = Utilities.getStringSharedPreferences(Utilities.mLanguageInputSharedPref , this);
        if (inputLanguage == null){
            Utilities.setStringSharedPreferences(Utilities.mLanguageInputSharedPref , SettingsActivity.Languages.mLanguages.get(0) , this );
            Utilities.setStringSharedPreferences(Utilities.mLanguageInputCodeSharedPref , SettingsActivity.Languages.mLanguagesCodes.get(0) , this);
            Utilities.setIntSharedPreferences(Utilities.mLanguageInputIdSharedPref , 0 , this);
            inputLanguage = SettingsActivity.Languages.mLanguages.get(0);
        }

        // set outputLanguages for default if the user has not choosed an output Language yet
        outputLanguage = Utilities.getStringSharedPreferences(Utilities.mLanguageOutputSharedPref , this);
        if(outputLanguage == null ) {
            Utilities.setStringSharedPreferences(Utilities.mLanguageOutputSharedPref  , SettingsActivity.Languages.mLanguages.get(1) , this);
            Utilities.setStringSharedPreferences(Utilities.mLanguageOutputCodeSharedPref , SettingsActivity.Languages.mLanguagesCodes.get(1) , this );
            Utilities.setIntSharedPreferences(Utilities.mLanguageOutputIdSharedPref , 1, this);
            outputLanguage = SettingsActivity.Languages.mLanguages.get(1);
        }


        // show for the input language selected
        mLanguageInputText = (TextView) findViewById(R.id.input_language_text_view);
        mLanguageInputText.setText(inputLanguage);

        // send the user to Settings Activity if he clicked on the view that shows the selected input language so that he can change it
        mLanguageInputText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (MainActivity.this , SettingsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        // show for the output language selected
        mLanguageOutputText = (TextView) findViewById(R.id.output_language_text_view);
        mLanguageOutputText.setText(outputLanguage);

        // send the user to Settings Activity if he clicked on the view that shows the selected output language so that he can change it
        mLanguageOutputText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (MainActivity.this , SettingsActivity.class);
                startActivity(intent);
                finish();
            }
        });


        // translate the input text
        mTranslateBtn = (Button) findViewById(R.id.translate_btn);
        mTranslateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //check the connectivity
                boolean internetConnection = isNetworkAvaliable(getBaseContext());
                // if the user is connected to the internet translated
                if (internetConnection){
                    // getting the input text from the EditText
                    String text = mEditText.getText().toString();


                    i = new Intent(getBaseContext(), TranslateService.class);

                    // send the input text with intent
                    i.putExtra("Edited", text);

                    // start translate service for translating the text
                    getBaseContext().startService(i);

                } else{
                    Toast.makeText(getApplicationContext(),"Sorry but you need to be connected to the internet", Toast.LENGTH_LONG)
                            .show();
                }

            }
        });


        // added a brodcast receiver that listen for the service if finished translating the text or not
        bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(RECEIVE_JSON);
        bManager.registerReceiver(bReceiver, intentFilter);



        // configuring the RecyclerView that will display a list of Translated Object
        this.mTranslatedRecyclerView = (RecyclerView) findViewById(R.id.translated_recyclerview);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        ((LinearLayoutManager) mLayoutManager).setReverseLayout(true);
        ((LinearLayoutManager) mLayoutManager).setStackFromEnd(true);
        this.mTranslatedRecyclerView.setLayoutManager(mLayoutManager);


        // getting a list of Translated object from the local database that will e displayed on RecyclerView
        List<Translated> translatedList = Utilities.getAllData(getContentResolver());

        // setting the getted list of Translated object to the RecyclerView
        this.mTranslatedAdapter = new TranslateAdapter(translatedList);
        this.mTranslatedRecyclerView.setAdapter(mTranslatedAdapter);


        // listening for the change in the local database and update the RecyclerView that shows the list of Translated Object
        getContentResolver().registerContentObserver(TranslateContract.TranslateEntry.CONTENT_URI , true, mContentObserver);
    }

    /**
     * This method is responsible for checking the internet connectivity
     * @param ctx
     * @return boolean
     */
    public static boolean isNetworkAvaliable(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if ((connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED)) {
            return true;
        } else {
            return false;
        }
    }


    // the broadcast receiver that listen to the change in translateService
    /**
     * This an example code of BroadCastReceiver
     */
    private BroadcastReceiver bReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(RECEIVE_JSON)) {
                String serviceJsonString = intent.getStringExtra("json");
                mTranslatedText.setText(serviceJsonString);
                context.stopService(i);
            }
        }
    };



    // this content observer listen for the changes in local databases for Translated object and updated the RecyclerView
    private ContentObserver mContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            Log.d("Test", "mContentObserver onChange");
            super.onChange(selfChange);
            List<Translated> translatedList = Utilities.getAllData(getContentResolver());

            mTranslatedAdapter = new TranslateAdapter(translatedList);
            mTranslatedRecyclerView.setAdapter(mTranslatedAdapter);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main , menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings){
            Intent intent = new Intent (MainActivity.this , SettingsActivity.class);
            startActivity(intent);
            finish();
        } else if (item.getItemId() == R.id.action_about){ }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bManager.unregisterReceiver(bReceiver);
        getContentResolver().unregisterContentObserver(mContentObserver);
    }

}
