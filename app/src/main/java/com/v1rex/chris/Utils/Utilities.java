package com.v1rex.chris.Utils;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.VisibleForTesting;

import com.v1rex.chris.Activities.SettingsActivity;
import com.v1rex.chris.Database.TranslateContract;
import com.v1rex.chris.Models.Translated;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public final class Utilities {

    public final static String mLanguageInputSharedPref = "LANGUAGE_INPUT_SHARED_PREF";

    public final static String mLanguageOutputSharedPref = "LANGUAGE_OUTPUT_SHARED_PREF";


    public final static String mLanguageInputCodeSharedPref = "LANGUAGE_INPUT_CODE_SHARED_PREF";

    public final static String mLanguageOutputCodeSharedPref = "LANGUAGE_OUTPUT_CODE_SHARED_PREF";


    public final static String mLanguageInputIdSharedPref = "LANGUAGE_INPUT_ID_SHARED_PREF";

    public final static String mLanguageOutputIdSharedPref = "LANGUAGE_OUTPUT_ID_SHARED_PREF";

    public Utilities() {
    }

    public static List<Translated> getAllData(ContentResolver contentResolver){
        List<Translated> list = new ArrayList<>();

        Cursor cursor = contentResolver
                .query(TranslateContract.TranslateEntry.CONTENT_URI,null,null,null,null);

        while (cursor.moveToNext()){
            int index = cursor.getColumnIndex(TranslateContract.TranslateEntry.COLUMN_ORIGINAL_TEXT);
            int index2 = cursor.getColumnIndex(TranslateContract.TranslateEntry.COLUMN_TRANSLATED_TEXT);
            int index3 = cursor.getColumnIndex(TranslateContract.TranslateEntry.COLUMN_ORIGINAL_LANGUAGE);
            int index4 = cursor.getColumnIndex(TranslateContract.TranslateEntry.COLUMN_FINAL_LANGUAGE);
            String originalText = cursor.getString(index);
            String translatedText = cursor.getString(index2);
            String originalLanguage = cursor.getString(index3);
            String finalLanguage = cursor.getString(index4);

            Translated translated = new Translated(originalText, translatedText, originalLanguage, finalLanguage);
            list.add(translated);
        }


        return list;
    }

    public static Uri insertNewRecord(Translated translated , ContentResolver contentResolver){
        ContentValues values = new ContentValues();

        values.put(TranslateContract.TranslateEntry.COLUMN_ORIGINAL_TEXT, translated.getmOriginalText());

        values.put(TranslateContract.TranslateEntry.COLUMN_TRANSLATED_TEXT, translated.getmTranslatedText());

        values.put(TranslateContract.TranslateEntry.COLUMN_ORIGINAL_LANGUAGE, translated.getmOrginalLanguage());

        values.put(TranslateContract.TranslateEntry.COLUMN_FINAL_LANGUAGE, translated.getmFinalLanguage());

        return contentResolver.insert(TranslateContract.TranslateEntry.CONTENT_URI , values);
    }

    public static String stringUrlEncode(String beEncoded){
        String text = "" ;
        try {
            text = URLEncoder.encode(beEncoded, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return text;
    }

    public static String stringUrlDecode(String beDecoded){
        String text = "" ;
        try {
            text = URLDecoder.decode(beDecoded, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return text;
    }

    public static String getStringSharedPreferences(String place , Context context){
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String finalString = preferences.getString(place , null);
        return finalString;
    }

    public static void setStringSharedPreferences (String place , String content , Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(place , content );
        editor.apply();
    }


    public static int getIntSharedPreferences(String place , Context context){
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        int finalString = preferences.getInt(place, -1);
        return finalString;
    }


    public static void setIntSharedPreferences (String place , int content , Context context){
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(place , content);
        editor.apply();
    }


}
