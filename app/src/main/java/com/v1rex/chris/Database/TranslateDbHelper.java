package com.v1rex.chris.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.v1rex.chris.Database.TranslateContract;

public class TranslateDbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "translatedList.db";


    private static final int DATABASE_VERSION = 1;

    public TranslateDbHelper(Context context){
        super(context, DATABASE_NAME, null , DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        /*
         * This String will contain a simple SQL statement that will create a table that will
         * cache our translate data.
         */
        final String SQL_CREATE_TODO_TABLE =

                "CREATE TABLE " + TranslateContract.TranslateEntry.TABLE_NAME + " (" +

                        /*
                         * TranslateEntry did not explicitly declare a column called "_ID". However,
                         * TranslateEntry implements the interface, "BaseColumns", which does have a field
                         * named "_ID". We use that here to designate our table's primary key.
                         */
                        TranslateContract.TranslateEntry._ID               + " INTEGER PRIMARY KEY AUTOINCREMENT, " +

                        TranslateContract.TranslateEntry.COLUMN_ORIGINAL_TEXT       + " TEXT NOT NULL, " +

                        TranslateContract.TranslateEntry.COLUMN_TRANSLATED_TEXT      + " TEXT NOT NULL, " +

                        TranslateContract.TranslateEntry.COLUMN_ORIGINAL_LANGUAGE       + " TEXT NOT NULL, "  +

                        TranslateContract.TranslateEntry.COLUMN_FINAL_LANGUAGE      + " TEXT NOT NULL);";

        /*
         * After we've spelled out our SQLite table creation statement above, we actually execute
         * that SQL with the execSQL method of our SQLite database object.
         */
        db.execSQL(SQL_CREATE_TODO_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TranslateContract.TranslateEntry.TABLE_NAME);
        onCreate(db);
    }
}
