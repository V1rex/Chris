package com.v1rex.chris.Database;

import android.net.Uri;
import android.provider.BaseColumns;

public class TranslateContract {

    /*
     * The "Content authority" is a name for the entire content provider, similar to the
     * relationship between a domain name and its website. A convenient string to use for the
     * content authority is the package name for the app, which is guaranteed to be unique on the
     * Play Store.
     */
    public static final String CONTENT_AUTHORITY = "com.v1rex.chris";


    /*
     * Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
     * the content provider for translate database.
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);



    /**
     * This class is not really necessary . But it keeps the code organized
     * and the general purpose of it: it is for defining the column name and the database name
     */
    public static final class TranslateEntry implements BaseColumns {
        // the table name for our table
        public static final String TABLE_NAME = "translated";


        /* Original text is stored as string representing the orginal text before it is translated */
        public static final String COLUMN_ORIGINAL_TEXT = "originalText";

        /* Final text is stored as string representing the final text after it is translated */
        public static final String COLUMN_TRANSLATED_TEXT = "translatedText";

        /* Original language name is stored as string representing the orginal text language before it is translated */
        public static final String COLUMN_ORIGINAL_LANGUAGE = "originalLanguage";

        /* Final language name is stored as string representing the final text language after it is translated */
        public static final String COLUMN_FINAL_LANGUAGE = "finalLanguage";


        /* The base CONTENT_URI used to query the translate table from the content provider */
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(TABLE_NAME)
                .build();

        /**
         * Builds a URI that adds the task _ID to the end of the translate content URI path.
         * This is used to query details about a single translate entry by _ID. This is what we
         * use for the detail view query.
         *
         * @param id Unique id pointing to that row
         * @return Uri to query details about a single translate entry
         */
        public static Uri buildTranslateUriWithId(long id) {
            return CONTENT_URI.buildUpon()
                    .appendPath(Long.toString(id))
                    .build();
        }


    }
}
