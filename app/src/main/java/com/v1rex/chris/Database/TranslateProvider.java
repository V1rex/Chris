package com.v1rex.chris.Database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * This class serves as the ContentProvider for all of Translate's data. This call allows us
 * to Insert data , query data and delete data.
 */


/**
 * This an example code of ContentProvider
 */
public class TranslateProvider extends ContentProvider {

    private TranslateDbHelper mOpenHelper;

    /**
     * In onCreate, we initialize our content provider on startup. This method is called for all
     * registered content providers on the application main thread at application launch time.
     * It must not perform lengthy operations, or application startup will be delayed.
     *
     * @return true if the provider was successfully loaded, false otherwise
     */
    @Override
    public boolean onCreate() {
        mOpenHelper = new TranslateDbHelper(getContext());
        return mOpenHelper != null;
    }


    /**
     * Handles query requests from clients. We will use this method to query for all
     * of our translate data as well as to query for the specific translate record.
     *
     * @param uri           The URI to query
     * @param projection    The list of columns to put into the cursor. If null, all columns are
     *                      included.
     * @param selection     A selection criteria to apply when filtering rows. If null, then all
     *                      rows are included.
     * @param selectionArgs You may include ?s in selection, which will be replaced by
     *                      the values from selectionArgs, in order that they appear in the
     *                      selection.
     * @param sortOrder     How the rows in the cursor should be sorted.
     * @return A Cursor containing the results of the query. In our implementation,
     */
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        Cursor cursor;


        switch (sUriMatcher.match(uri)) {

            case CODE_TRANSLATE_WITH_ID: {

                String _ID = uri.getLastPathSegment();


                String[] selectionArguments = new String[]{_ID};

                cursor = mOpenHelper.getReadableDatabase().query(

                        TranslateContract.TranslateEntry.TABLE_NAME,
                        projection,
                        TranslateContract.TranslateEntry._ID + " = ? ",
                        selectionArguments,
                        null,
                        null,
                        sortOrder);

                break;
            }

            case CODE_TRANSLATE: {
                cursor = mOpenHelper.getReadableDatabase().query(
                        TranslateContract.TranslateEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    /**
     * Handles request to insert a new row.
     *
     * @param uri    The URI of the insertion request. This must not be null.
     * @param values A set of column_name/value pairs to add to the database. This must not be null
     * @return       The URI for the newly inserted item.
     */


    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        switch (sUriMatcher.match(uri)) {
            case CODE_TRANSLATE:

                long _id = db.insert(TranslateContract.TranslateEntry.TABLE_NAME, null, values);

                /* if _id is equal to -1 insertion failed */
                if (_id != -1) {
                    /*
                     * This will help to broadcast that database has been changed,
                     * and will inform entities to perform automatic update.
                     */
                    getContext().getContentResolver().notifyChange(uri, null);
                }

                return TranslateContract.TranslateEntry.buildTranslateUriWithId(_id);

            default:
                return null;
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }



    /*
     * These constant will be used to match URIs with the data we are looking for. We will take
     * advantage of the UriMatcher class to make that matching MUCH easier than doing something
     * ourselves, such as using regular expressions.
     */
    public static final int CODE_TRANSLATE = 100;
    public static final int CODE_TRANSLATE_WITH_ID = 101;

    /* The URI Matcher used by this content provider. */
    private static final UriMatcher sUriMatcher = buildUriMatcher();

    /**
     * Creates the UriMatcher that will match each URI to the CODE_TTRANSLATE and
     * CODE_TTRANSLATE_WITH_ID constants defined above.
     *
     * UriMatcher does all the hard work for you. You just have to tell it which code to match
     * with which URI, and it does the rest automatically.
     *
     * @return A UriMatcher that correctly matches the constants for CODE_TODO and CODE_TODO_WITH_ID
     */
    public static UriMatcher buildUriMatcher() {


        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = TranslateContract.CONTENT_AUTHORITY;


        matcher.addURI(authority, TranslateContract.TranslateEntry.TABLE_NAME, CODE_TRANSLATE);

        matcher.addURI(authority, TranslateContract.TranslateEntry.TABLE_NAME + "/#", CODE_TRANSLATE_WITH_ID);

        return matcher;
    }

}
