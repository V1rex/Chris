package com.v1rex.chris.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.v1rex.chris.Models.Translated;
import com.v1rex.chris.R;

import java.util.List;

// this code is responsible for displaying a list of Translated object to a RecyclerView that displays a list of old translated input texts
public class TranslateAdapter extends RecyclerView.Adapter<TranslateAdapter.ViewHolder> {

    private List<Translated> translatedList;

    // initialise the adapter with a List of Translated Object
    public TranslateAdapter(List<Translated> translatedList) {
        this.translatedList = translatedList;
    }

    @NonNull
    @Override
    public TranslateAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.translated_layout, parent, false);

        return new ViewHolder(v);
    }

    // bind variables in Translated object to a view and dispaly the content of it
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Translated translated = translatedList.get(position);

        holder.mOrignalTextView.setText(translated.getmOriginalText());
        holder.mFinalTextView.setText(translated.getmTranslatedText());

        holder.mOriginalLanguageTextView.setText(translated.getmOrginalLanguage());
        holder.mFinalLanguageTextView.setText(translated.getmFinalLanguage());
    }

    @Override
    public int getItemCount() {
        if(translatedList != null){
            return translatedList.size();
        }else {
            return 0;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView mOrignalTextView;

        public TextView mFinalTextView;

        public TextView mOriginalLanguageTextView;

        public TextView mFinalLanguageTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            // displaying text on Views
            mOrignalTextView = itemView.findViewById(R.id.original_text_view);
            mFinalTextView = itemView.findViewById(R.id.final_text_view);

            mOriginalLanguageTextView = itemView.findViewById(R.id.original_language_text_view);
            mFinalLanguageTextView = itemView.findViewById(R.id.final_language_text_view);
        }
    }
}
