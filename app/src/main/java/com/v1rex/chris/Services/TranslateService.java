package com.v1rex.chris.Services;


import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.v1rex.chris.Activities.MainActivity;
import com.v1rex.chris.Activities.SettingsActivity;
import com.v1rex.chris.Database.TranslateContract;
import com.v1rex.chris.Models.Translated;
import com.v1rex.chris.Utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;


/**
 * This an example code of Service
 */
public class TranslateService extends Service {

    private final String url = "https://translate.yandex.net/api/v1.5/tr.json/translate?";
    private final String api_key = "key=trnsl.1.1.20180903T143417Z.39620076f097d8e8.9bb1a12723282a35c44439225a9883714683ab0b";
    private String text = "&text=" ;

    private Translated translated;



    /**
     *
     * This onStartCommand method is called when the service is startted
     * This method is responsible for translating the text by getting input and output language for sharedPreferences that stores users settigns
     * then transform the input text that the user wanna translate to a URLENCODED so that we can make an HTTP reuest with it
     * and call finally translateText method responsible for making the web request
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // this line of code is responsible for getting the input text sended from the activity that the user wanna translate
        String jsonString = intent.getStringExtra("Edited");


        // the following line of code get the input and output langugae setted by the user in SettingsActivity
        // SettingsActivity stores the users choices of language in a SharedPreferences
        // their is four variables that is stored on the SharedPreferences :
        // two strings that stores the input and output code languages
        // two int that stores the input and output languages id : the id is used for getting the language code from an ArrayList of CodeLanguage or LanguagesName


        String originalLanguage = Utilities.getStringSharedPreferences(Utilities.mLanguageInputCodeSharedPref , this);
        String finalLanguage = Utilities.getStringSharedPreferences(Utilities.mLanguageOutputCodeSharedPref, this);

        int originalLanguageId = Utilities.getIntSharedPreferences(Utilities.mLanguageInputIdSharedPref , this);

        int finalLanguageId = Utilities.getIntSharedPreferences(Utilities.mLanguageOutputIdSharedPref , this);


        //this line is reponsible for regrouping the selected languages in a string that will be used by http request
        String language = "&lang=" + originalLanguage + "-" + finalLanguage ;

        // in a web request a string should be converted to a URLEncoded string so that the web request can work
        String Text = Utilities.stringUrlEncode(jsonString);

        // after getting all the informations needed the onStartCommand will call translateText method
        try {
            translateText(Text, language , originalLanguageId , finalLanguageId);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return Service.START_STICKY;
    }

    /**
     * This method is reponsible for making web reuest and getting result
     * @param text : the original text that the user wanna translate
     * @param language : a string that select the input and output language for the text
     * @param originalLanguageId : the id of the inputLanguage used from an ArrayList : so that it will be easy to select the language
     * @param finalLanguageId : the id of the outputLanguage used from an ArrayList : so that it will be easy to select the language
     * @return Translated object wich will be added to the local database
     * read the documentation pf yandex translate to see how i've maked the web request : https://tech.yandex.com/translate/doc/dg/concepts/About-docpage/
     * @throws UnsupportedEncodingException
     */
    private void translateText(final String text, String language, final int originalLanguageId , final int finalLanguageId) throws UnsupportedEncodingException {

        // this a string for the url that will be making with it a web request
        String baseUrl = url + api_key + language + this.text + text ;


        final String[] translatedText = {""};

        // after making a web request it return a json string that we gonna decode to get values returned from the web service
        RequestQueue request = Volley.newRequestQueue(getApplicationContext());
        final JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, baseUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    // getting the output translated text
                    JSONArray jsonArray = response.getJSONArray("text");
                    translatedText[0] = jsonArray.get(0).toString();

                    String translatedTextFinal = translatedText[0];
                    String originalText = text;

                    // decoding the URLEncoded string returned from the web request
                    translatedTextFinal = Utilities.stringUrlDecode(translatedTextFinal);
                    originalText = Utilities.stringUrlDecode(originalText) ;

                    // Send a message to activity: that the web request has finished and that the text has been translated
                    Intent RTReturn = new Intent(MainActivity.RECEIVE_JSON);
                    RTReturn.putExtra("json", translatedTextFinal);
                    LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(RTReturn);

                    // intisialising a Translated object for adding it to te database
                    translated = new Translated(originalText , translatedTextFinal , SettingsActivity.Languages.mLanguages.get(originalLanguageId), SettingsActivity.Languages.mLanguages.get(finalLanguageId) );
                    // adding the object to the database
                    Utilities.insertNewRecord(translated, getContentResolver());

                } catch (JSONException e) {
                    Log.v("KEY", "ERR: " + e.getLocalizedMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.v("KAT", "ERR: " + error.toString());
            }
        });

        // making the web request with volley : wich is a library made by google for making web requests
        request.add(jsonRequest);

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
