package com.v1rex.chris.Models;

public class Translated {

    private String mOriginalText ;

    private String mTranslatedText ;

    private String mOrginalLanguage ;

    private String mFinalLanguage ;

    // constructor
    public Translated(String mOriginalText, String mTranslatedText, String mOrginalLanguage, String mFinalLanguage) {
        this.mOriginalText = mOriginalText;
        this.mTranslatedText = mTranslatedText;
        this.mOrginalLanguage = mOrginalLanguage;
        this.mFinalLanguage = mFinalLanguage;
    }


    /**
     * getters and setters
     * @return Strings
     */
    public String getmOriginalText() {
        return mOriginalText;
    }

    public String getmTranslatedText() {
        return mTranslatedText;
    }


    public String getmOrginalLanguage() {
        return mOrginalLanguage;
    }

    public String getmFinalLanguage() {
        return mFinalLanguage;
    }

}
