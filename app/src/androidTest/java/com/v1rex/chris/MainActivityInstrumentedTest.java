package com.v1rex.chris;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.InstrumentationTestCase;
import android.text.TextUtils;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;

import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


import com.v1rex.chris.Activities.MainActivity;
import com.v1rex.chris.Activities.SettingsActivity;
import com.v1rex.chris.Models.Translated;
import com.v1rex.chris.Utils.Utilities;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentedTest extends InstrumentationTestCase {

    private Context instrumentationCtx;
    private ContentResolver contentResolver;
    private String inputLanguage = "" ;
    private String outputLanguage = "" ;
    @Rule
    public ActivityTestRule <MainActivity> mainActivityActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setup(){
        instrumentationCtx = InstrumentationRegistry.getContext();
        contentResolver = instrumentationCtx.getContentResolver();
    }

    @Test
    public void check_InputLanguageSharedPrefrencesisNotNull(){
        inputLanguage = Utilities.getStringSharedPreferences(Utilities.mLanguageInputSharedPref  , instrumentationCtx);
        assertNotNull(TextUtils.isEmpty(inputLanguage));
    }

    @Test
    public void check_OutputLanguageSharedPrefrencesisNotNull(){
        outputLanguage = Utilities.getStringSharedPreferences(Utilities.mLanguageOutputSharedPref , instrumentationCtx);
        assertNotNull(TextUtils.isEmpty(outputLanguage));
    }

    @Test
    public void set_InputLanguageSharedPreferencesToDefaultTest(){
        Utilities.setStringSharedPreferences(Utilities.mLanguageInputSharedPref , SettingsActivity.Languages.mLanguages.get(0) , instrumentationCtx );
        Utilities.setStringSharedPreferences(Utilities.mLanguageInputCodeSharedPref , SettingsActivity.Languages.mLanguagesCodes.get(0) , instrumentationCtx);
        Utilities.setIntSharedPreferences(Utilities.mLanguageInputIdSharedPref , 0 , instrumentationCtx);
        inputLanguage = Utilities.getStringSharedPreferences(Utilities.mLanguageInputSharedPref  , instrumentationCtx);
        assertEquals(inputLanguage , SettingsActivity.Languages.mLanguages.get(0) );
    }

    @Test
    public void set_OutputLanguageSharedPreferencesToDefaultTest(){
        Utilities.setStringSharedPreferences(Utilities.mLanguageOutputSharedPref  , SettingsActivity.Languages.mLanguages.get(1) , instrumentationCtx);
        Utilities.setStringSharedPreferences(Utilities.mLanguageOutputCodeSharedPref , SettingsActivity.Languages.mLanguagesCodes.get(1) , instrumentationCtx );
        Utilities.setIntSharedPreferences(Utilities.mLanguageOutputIdSharedPref , 1, instrumentationCtx);
        outputLanguage = Utilities.getStringSharedPreferences(Utilities.mLanguageOutputSharedPref , instrumentationCtx);
        assertEquals(outputLanguage , SettingsActivity.Languages.mLanguages.get(1));
    }

    @Test
    public void contentProviderTest(){
        Translated translated = new Translated("hello", "bonjour", "English" , "French");
        Utilities.insertNewRecord(translated , contentResolver);
        List<Translated> translatedList = Utilities.getAllData(contentResolver);
        Translated latestTranslated = translatedList.get(translatedList.size() - 1);
        assertEquals(latestTranslated.getmOriginalText() , translated.getmOrginalLanguage());
        assertEquals(latestTranslated.getmTranslatedText() , translated.getmTranslatedText());
    }



    @Test
    public void translateTextFromActivityTest() throws InterruptedException {
        onView(withId(R.id.edit_text)).perform(typeText("hello"), closeSoftKeyboard());

        onView(withId(R.id.translate_btn)).perform(click());

        Thread.sleep(10000);

        onView(withId(R.id.edit_translated_text)).check(matches(withText("bonjour")));
    }


}
